class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.integer :uid
			t.string :email

			t.string :account_name

      t.timestamps
    end
  end
end
