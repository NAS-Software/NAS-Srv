require 'omniauth-oauth2'

module OmniAuth
	module Strategies
		class NasAuth < OAuth2
			# change the class name and the :name option to match your application name
			option :name, :nas_auth

			option :client_options, Settings.nas_auth.client

			uid { raw_info["id"] }

			info do
				{
						:email => raw_info["email"]
						# and anything else you want to return to your API consumers
				}
			end

			def raw_info
				@raw_info ||= access_token.get('/api/info').parsed
			end

			# https://github.com/intridea/omniauth-oauth2/issues/81
			def callback_url
				full_host + script_name + callback_path
			end
		end
	end
end
