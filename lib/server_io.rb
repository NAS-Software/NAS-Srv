require 'shellwords'

module ServerIO
	class ServerIO
		def initialize
			@ops = []
		end

		def create_user(username)
			@ops << CreateUserOp.new(username)
		end

		def set_password(username, password)
			@ops << ChangePasswordOp.new(username, password)
		end

		def exec!
			s = Settings.target

			Net::SSH.start(
					s.host, s.user, port: s.port, keys: [s.private_key],
					auth_methods: ['publickey'], keys_only: true
			) do |ssh|
				@ops.each do |o|
					o.apply ssh
				end
			end
		end
	end

	class Op
		def apply(ssh)
		end

		def run(ssh, cmd)
			if Rails.env.production?
				ssh.exec! cmd
			else
				puts 'Would run: ' + cmd
			end
		end
	end

	class CreateUserOp < Op
		def initialize(username)
			@username = username
		end

		def apply(ssh)
			run ssh, "useradd -m #{@username.shellescape}"
		end
	end

	class ChangePasswordOp < Op
		def initialize(username, password)
			@username = username
			@password = password
		end

		def apply(ssh)
			params = @username + ':' + @password
			run ssh, "echo #{params.shellescape} | chpasswd"
		end
	end
end
