class User < ApplicationRecord

	def session
		{
			id: id
		}
	end

	def has_account?
		account_name != nil
	end

	def title
		has_account? ? account_name : email
	end

	def self.from_omni(info)
		return find_or_create_by uid: info.uid do |u|
			u.email = info.info.email
		end
	end

	def self.from_session(info)
		return find_by! id: info['id']
	end
end
