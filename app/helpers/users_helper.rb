module UsersHelper
	def current_user
		return nil if session[:user] == nil
		User.from_session session[:user]
	end
end
