require 'server_io'

class UsersController < ApplicationController
	skip_before_action :verify_authenticity_token, only: :login

	def login
		@user = User.from_omni request.env['omniauth.auth']
		session[:user] = @user.session
		redirect_to user_path
	end

	def logout
		session[:user] = nil
		redirect_to '/'
	end

	def show
		auth_user
	end

	def reset_password
		return unless verify_user_status true

		password = random_password
		io = ServerIO::ServerIO.new
		io.set_password @user.account_name, password
		io.exec!
		redirect_to user_path, notice: 'Your temporary password is ' + password
	end

	def setup
		return unless verify_user_status false

		username = params[:create][:username]
		password = random_password

		io = ServerIO::ServerIO.new
		io.create_user username
		io.set_password username, password

		if /^[a-z]*$/.match username
			@user.account_name = username
			@user.transaction do
				@user.save!
				io.exec!
			end
			redirect_to user_path, notice: 'Your account has been created. Your temporary password is ' + password
		else
			redirect_to user_path, alert: 'Your username must only contain lower case letters.'
		end
	end

	private
	def random_password
		[*('a'..'z'), *('0'..'9')].shuffle[0, 16].join
	end

	def verify_user_status(registered)
		return unless auth_user
		if @user.has_account? != registered
			redirect_to '/'
			return false
		end
		true
	end

end
