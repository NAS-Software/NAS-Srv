class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception

	def auth_user
		if session[:user] == nil
			redirect_to '/auth/nas_auth'
			return nil
		end
		@user = User.from_session session[:user]
	end
end
