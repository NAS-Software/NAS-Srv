module OmniAuth
	module Strategies
		# tell OmniAuth to load our strategy
		autoload :NasAuth, 'nasauth'
	end
end

Rails.application.config.middleware.use OmniAuth::Builder do
	conf = Settings.nas_auth.appconf
	provider :NasAuth, conf.appid, conf.secret
	provider :developer unless Rails.env.production?
end
