Rails.application.routes.draw do

	root 'homepage#index'

	match '/auth/:provider/callback', to: 'users#login', via: [:get, :post]
	get '/logout', to: 'users#logout' # TODO: move to delete method

	resource :user, only: [:show, :update] do
		member do
			post 'setup'
			post 'reset_password'
		end
	end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
